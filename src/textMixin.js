export const textMixin = {
  data() {
    return {
        text: ''
    }
  },
  computed: {
      reversedText() {
          return this.text.split("").reverse().join("");
      },
      textWithLenght() {
          let value = this.text;
          return `${value} (${value.length})`;
      }
  }
}